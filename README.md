# GeneticKnapsack

## Introduction
It's a python app implementing genetic algorithm for solving knapsack problem.  

### Structure
data - contains example datasets 
analysis.ipynb - notebook for result analysis
res_* files - results for given input data
geneticknapsack.py - main file to run
