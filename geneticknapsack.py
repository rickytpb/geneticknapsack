import numpy as np 
import pandas as pd
import os 
import sys 

DATADIR = 'data'
PREFIX = 'large_scale'
DATASETS_100 = ['knapPI_1_100_1000_1','knapPI_2_100_1000_1']
DATASETS_500 = ['knapPI_1_500_1000_1','knapPI_2_500_1000_1']
DATASETS_1000 = ['knapPI_1_1000_1000_1','knapPI_2_1000_1000_1']


def load_data(fname):
    # Utility function
    # Reads the data to a tuple of numpy arrays 
    # and two int values
    weights = []
    gains = []
    optimal = []
    with open(fname,'r') as f:
        info = f.readline().split(' ')
        n = int(info[0].strip())
        wmax = int(info[1].strip())
        for line in f.readlines():
            line=line.split(' ')
            if len(line) == 2:
                weight = int(line[0].strip())
                gain = int(line[1].strip())
                weights.append(weight)
                gains.append(gain)
            if len(line) > 2:
                for item in line:
                    optimal.append(int(item.strip()))
    return np.array(weights), np.array(gains), np.array(optimal), wmax, n

def sum_of_product(arr, arr2):
    # Simple wrapper function to get 
    # Sum of dot product
    return np.sum(arr*arr2)

def get_greedy_probs(ws, gs):
    # Get probabilites calculated from
    # Relative gain (gain per weight unit)
    relative_gain = gs/ws
    return relative_gain/np.sum(relative_gain)

def get_chromosome(size, method='random', probs=None):
    # Produces a single chromosome of size s
    # It's randomized with a probability factor
    # Drastically higher chance of getting a 0 
    # So it doesn't go above the weight limit instantly
    chromosome = np.zeros((size,1))
    if method == 'random':
        chromosome = np.random.choice([0, 1], size=size, p=[99/100.,1/100.])
    if method == 'greedy':
        chromosome = np.zeros((size,), dtype=np.int16)
        chromosome[np.random.choice(np.arange(probs.size),p=probs)] = 1
        chromosome[np.random.choice(np.arange(probs.size),p=probs)] = 1
        chromosome[np.random.choice(np.arange(probs.size),p=probs)] = 1
    return chromosome

def start_population(chromosome_size,population_size=100, method='random', probs=None):
    # Produces a population of specific size
    # Consisting chromosomes of chosen size
    population = []
    while len(population) < population_size:
        population.append(get_chromosome(chromosome_size,method,probs))
    return np.array(population,dtype=np.int16)

def evaluate_parents(population, weights, values, wmax, threshold=0):
    # Function used to select best parents from population
    ws = population * weights 
    vs = population * values
    kp_weights = ws.sum(axis=1)
    kp_values = vs.sum(axis=1)
    invalid_idx = np.where(kp_weights>wmax)
    kp_values[invalid_idx] = 0
    invalid_idx = np.where(kp_values<threshold)
    kp_values[invalid_idx] = 0
    return kp_values

def mutate(child, alpha=0.5, method='simple'):
    # Function used to mutate children
    # Method simple: set random points to 0
    # Method recursive: same as simple but with recursion
    chance = np.random.random()
    if chance>alpha:
        idx = np.random.randint(0,child.shape[0],1)
        if method=='recursive':
            child[idx]= np.abs(child[idx]-1)
            return mutate(child)
        if child[idx]==1:
            child[idx]=0
    return child 

def reproduce(pop_to_repr,offset=15):
    # Single point crossover
    s = pop_to_repr[0].shape[0]
    children = []
    for i in range(pop_to_repr.shape[0]):
        for j in range(pop_to_repr.shape[0]):
            if i == j :
                continue
            crossover_point = np.random.choice(np.arange(1+offset,s-1-offset,1),1)[0]
            l_x = pop_to_repr[i][:crossover_point]
            l_y = pop_to_repr[j][crossover_point:]
            r_x = pop_to_repr[i][crossover_point:]
            r_y = pop_to_repr[j][:crossover_point]
            child_l = np.concatenate((l_x,l_y))
            child_l = mutate(child_l,method='recursive')
            child_r = np.concatenate((r_x,r_y))
            child_r = mutate(child_r,method='recursive')
            children.append(child_l)
            children.append(child_r)
    return np.array(children)

def adjust_children_size(children, pop_size, dim_n, upsample_method='random', probs=None):
    #Adjust the children to match the size of the previous population
    #Upsampling with randomness
    #Downsampling - cut after shuffle
    if len(children) > pop_size:
        np.random.shuffle(children)
        return children[:pop_size]
    if len(children) < pop_size:
        new_children = start_population(dim_n,np.abs(len(children)-pop_size),upsample_method,probs)
        children = np.concatenate((children,new_children))
        np.random.shuffle(children)
        return np.concatenate((children,new_children))
    return children
        
def get_best_entity(pop, ws,gs,wmax):
    # Gets best combination from the set
    # Used only for evaluation
    ws = pop * ws 
    vs = pop * gs
    kp_weights = ws.sum(axis=1)
    kp_values = vs.sum(axis=1)
    invalid_idx = np.where(kp_weights>wmax)
    kp_values[invalid_idx] = 0
    best_idx = np.argpartition(kp_values, -1)[-1:]
    return kp_values[best_idx]

if __name__ == "__main__":
    for dataset in DATASETS_1000:
        for i in range(10):
            print(f"Processing problem {dataset}, iteration {i}")
            f = os.path.join(DATADIR,PREFIX,dataset)
            generations = 100
            pop_size = 1000
            best_pct = 0.1
            stability_param = 4
            sel_size = int(best_pct*pop_size)
            gs, ws, sol, wmax, dim_n = load_data(f)
            greedy_probs = get_greedy_probs(ws,gs)
            pop = start_population(dim_n,pop_size,'greedy',greedy_probs)
            best_val = 0
            last_vals = []
            for i in range(generations):
                last_vals.append(best_val)
                parent_vals = evaluate_parents(pop,ws,gs,wmax)
                to_reproduce = np.argpartition(parent_vals, -sel_size)[-sel_size:]
                pop_to_reproduce = pop[to_reproduce]
                if pop_to_reproduce.shape[0] == 0:
                    raise RuntimeError()
                children = reproduce(pop_to_reproduce)
                pop = adjust_children_size(children,pop_size,dim_n)
                best_val = get_best_entity(pop,ws,gs,wmax)
                #SIMULATE DISASTER IF TOO STABLE
                #AND WITH BINARY MASK
                if last_vals[-stability_param:].count(best_val) == stability_param:
                    #pop = adjust_children_size(pop[:sel_size],pop_size,dim_n,'greedy',greedy_probs)
                    mask = np.random.choice([0, 1], size=dim_n, p=[90/100,10/100.])
                    pop = pop*mask
            with open(f'res_{dataset}.txt','a+') as f:
                f.write(f"{max(last_vals)[0]}\n")
